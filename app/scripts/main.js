$(document).ready(function() {
    $(window).on("scroll", function () {
        if ($(this).scrollTop() > 100) {
            $("header").css("background", "#d794f2");
        }
        else {
            $("header").css("background", "transparent");
        }
    });
});