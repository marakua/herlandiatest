import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import browserSync from 'browser-sync';
import del from 'del';
import fileinclude from 'gulp-file-include';

var paths = {
    scripts: {
        input: 'app/scripts/**/*.js',
        tmp: '.tmp/scripts/',
        output: 'dist/scripts/'
    },
    styles: {
        input: 'app/styles/**/*.scss',
        tmp: '.tmp/styles/',
        output: 'dist/styles/'
    },
    html: {
        input: 'app/**/*.html',
        tmp: '.tmp/',
        output: 'dist/'
    },
    images: {
        input: 'app/images/**/*.{jpe?g,png,svg,gif,ico}',
        tmp: '.tmp/images',
        output: 'dist/images/'
    },
    fonts: {
        input: 'app/fonts/**/*.{ttf,woff,eof,svg}',
        tmp: '.tmp/fonts',
        output: 'dist/fonts/'
    },
    misc: {
        input: 'app/*.{ico,png,txt,xml,json}',
        tmp: '.tmp/',
        output: 'dist/'
    }
};

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

gulp.task('styles', () => {
    return gulp.src(paths.styles.input)
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.sass.sync({
            outputStyle: 'expanded',
            precision: 10,
            includePaths: ['node_modules']
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer({browsers: ['> 1%', 'last 3 versions', 'Firefox ESR']}))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(paths.styles.tmp))
        .pipe(reload({stream: true}));
})


gulp.task('build:styles', () => {
    return gulp.src(paths.styles.input)
        .pipe($.plumber())
        .pipe($.sass.sync({
            outputStyle: 'expanded',
            precision: 10,
            includePaths: ['node_modules']
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer({browsers: ['> 1%', 'last 3 versions', 'Firefox ESR']}))
        .pipe(gulp.dest(paths.styles.output))
})


gulp.task('scripts', () => {
    return gulp.src([paths.scripts.input, '!node_modules/**'])
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.babel({presets: ['es2015']}))
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(paths.scripts.tmp))
        .pipe(reload({stream: true}));
})


gulp.task('lint:scripts', () => {
    return gulp.src([paths.scripts.input, '!node_modules/**'])
        .pipe($.plumber())
        .pipe($.eslint({
            extends: "airbnb"
        }))
        .pipe($.eslint.format())
        .pipe(reload({stream: true}));
})


gulp.task('fileinclude', () => {
    return gulp.src(paths.html.input)
        .pipe($.plumber())
        .pipe(fileinclude())
        .pipe(gulp.dest(paths.html.tmp))
        .pipe(browserSync.stream());
})


gulp.task('html', ['build:styles', 'scripts'], () => {
    return gulp.src([paths.html.input, '!app/partials/**'])
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', $.cssnano()))
        .pipe($.if('*.html', fileinclude()))
        // .pipe($.if('*.html', $.htmlmin({collapseWhitespace: true})))
        .pipe($.useref({searchPath: ['node_modules', 'app', 'bower_components']}))
        .pipe(gulp.dest(paths.html.output));
})


gulp.task('images', () => {
    return gulp.src(paths.images.input)
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true,
            // don't remove IDs from SVGs, they are often used
            // as hooks for embedding and styling
            svgoPlugins: [{cleanupIDs: false}]
        })))
        .pipe(gulp.dest(paths.images.output));
})


gulp.task('fonts', () => {
    return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {
    })
        .concat('app/fonts/**/*'))
        .pipe(gulp.dest('.tmp/fonts'))
        .pipe(gulp.dest('dist/fonts'));
})


gulp.task('extras', () => {
    return gulp.src([
        'app/*.*',
        '!app/*.html',
        '!app/partials/*.html'
    ], {
        dot: true
    }).pipe(gulp.dest('dist'));
})


gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('serve', ['fileinclude', 'images', 'styles', 'scripts', 'fonts'], () => {
    browserSync({
        notify: false,
        port: 9000,
        server: {
            baseDir: ['.tmp', 'app', 'node_modules', 'bower_components']
        }
    });


gulp.watch([
    'app/*.html',
    'app/images/**/*',
    '.tmp/fonts/**/*'
]).on('change', reload);

gulp.watch('app/**/*.html', ['fileinclude']);
gulp.watch('app/styles/**/*.scss', ['styles']);
gulp.watch('app/scripts/**/*.js', ['scripts']);
gulp.watch('app/fonts/**/*', ['fonts']);
gulp.watch('bower.json', ['wiredep', 'fonts']);
})


gulp.task('build', ['html', 'images', 'fonts', 'extras'], () => {
    return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
})


gulp.task('default', ['clean'], () => {
    gulp.start('build');
})

